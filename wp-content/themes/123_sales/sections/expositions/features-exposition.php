<section class="section featuresexposition">
	<div class="section-wrapper featuresexposition-wrapper">
		<h1 class="section-wrapper-header featuresexposition-wraapper-header">Features</h1>
		<?php if( have_rows('features', 'option') ): ?>
			<div class="featuresexposition-wrapper-grid">
				<?php 
				$counter = 0;	
				while( have_rows('features', 'option') ): the_row(); ?>
					<div class="featuresexposition-wrapper-grid-item<?php echo $counter % 2 == 0 ? '' : ' invert'; ?>">
						<div class="featuresexposition-wrapper-grid-item-left">
							<img src="<?php the_sub_field('image'); ?>" class="featuresexposition-wrapper-grid-item-left-image">
						</div>
						<div class="featuresexposition-wrapper-grid-item-right">
							<h2 class="featuresexposition-wrapper-grid-item-right-header"><?php the_sub_field('title'); ?></h2>
							<div class="featuresexposition-wrapper-grid-item-right-description"><?php the_sub_field('description'); ?></div>
						</div>
					</div>
				<?php 
				$counter++;
				endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</section>