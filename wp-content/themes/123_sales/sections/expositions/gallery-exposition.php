<section class="section galleryexposition">
	<div class="section-wrapper galleryexposition-wrapper">
		<h1 class="section-wrapper-header galleryexposition-filters-wraapper-header">Site Gallery</h1>
		<div class="galleryexposition-filters">
			<div class="galleryexposition-filters-filter">
				<h3 class="galleryexposition-filters-filter-label">Filter by Category: </h3>
				<select class="galleryexposition-filters-filter-ui" name="Category">
					<option class="galleryexposition-filters-filter-ui-item"></option>
					<?php 
						$terms = get_terms( array(
							'taxonomy' => 'gallery_category',
						) );
						foreach( $terms as $term ): ?>
							<option class="galleryexposition-filters-filter-ui-item" data-id="<?php echo $term->term_id ?>"><?php echo $term->name; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="galleryexposition-wrapper-grid">
			<?php 
				$posts = UtilityBelt::get_all_gallery_site_meta(0, 9);
				foreach( $posts as $post ):
			?>
				<div class="galleryexposition-wrapper-grid-item<?php foreach($post['categories'] as $cat) echo ' ' . $cat->term_id;  ?>" style="background-image: url('<?php echo $post['image']; ?>');" data-url="<?php echo $post['url']; ?>">	
					<div class="galleryexposition-wrapper-grid-item-cats">
					<?php 
						foreach($post['categories'] as $cat):?>
						<div class="galleryexposition-wrapper-grid-item-cats-cat"><?php echo $cat->name ?></div>
					<?php
						endforeach;
					?>
					</div>
					<div class="galleryexposition-wrapper-grid-item-text"><?php echo $post['title'] ?></div>
					<div class="galleryexposition-wrapper-grid-item-tint"></div>
				</div>
			<?php 
				endforeach;
			?>
		</div>
		<div class="galleryexposition-wrapper-loading hide">
			<i class="fa fa-spinner"></i>
		</div>
	</div>
</section>
<?php 

$modal = true;

include(locate_template('components/gallerysite.php' )); 

?>