<section class="section gallerysummary<?php echo $index % 2 == 0 ? ' grey' : ''; ?>">
	<div class="section-wrapper gallerysummary-wrapper">
		<h3 class="section-wrapper-superheader"><?php the_field('gallery_superheader', 'option'); ?></h3>
		<h1 class="section-wrapper-header gallerysummary-wrapper-header"><?php the_field('gallery_header', 'option'); ?></h1>

		<div class="gallerysummary-wrapper-grid">
			<?php 
				$rows = UtilityBelt::get_all_gallery_site_meta();
				foreach( $rows as $row ):
					if( $row['on-home-page'] ):
			?>
				<div class="gallerysummary-wrapper-grid-item" style="background-image: url('<?php echo $row['image']; ?>');" data-url="<?php echo $row['url']; ?>">	
					<div class="gallerysummary-wrapper-grid-item-text"><?php echo $row['title'] ?></div>
					<div class="gallerysummary-wrapper-grid-item-tint"></div>
				</div>
			<?php 
					endif;
				endforeach;
			?>
		</div>

		<a href="<?php echo site_url( '/gallery/' ); ?>" class="gallerysummary-wrapper-button section-wrapper-button">See all sites</a>
	</div>
</section>
<?php 

$modal = true;

include(locate_template('components/gallerysite.php' )); 

?>