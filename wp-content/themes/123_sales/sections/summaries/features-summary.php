<?php 
	if( have_rows('features', 'option') ):
?>
<section class="section featuressummary<?php echo $index % 2 == 0 ? ' grey' : ''; ?>">
	<div class="section-wrapper featuressummary-wrapper">
		<h3 class="section-wrapper-superheader"><?php the_field('features_superheader', 'option'); ?></h3>
		<h1 class="section-wrapper-header featuressummary-wraapper-header"><?php the_field('features_header', 'option'); ?></h1>
		<div class="featuressummary-wrapper-grid">
		<?php while( have_rows('features', 'option') ): the_row(); 
			if( get_sub_field('on-home-page') ):
			?>
			<div class="featuressummary-wrapper-grid-item">
				<div class="featuressummary-wrapper-grid-item-left">
					<img class="featuressummary-wrapper-grid-item-left-image" src="<?php the_sub_field('image'); ?>">
				</div>
				<div class="featuressummary-wrapper-grid-item-right">
					<h2 class="featuressummary-wrapper-grid-item-right-header"><?php the_sub_field('title'); ?></h2>
					<div class="featuressummary-wrapper-grid-item-right-description">
						<?php the_sub_field('description') ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php endwhile; ?>
		</div>
		<a href="<?php echo site_url( '/features/' ); ?>" class="featuressummary-wrapper-button section-wrapper-button">See all features</a>
	</div>
</section>
<?php endif; ?>