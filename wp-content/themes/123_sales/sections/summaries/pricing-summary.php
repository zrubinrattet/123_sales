<section class="section pricingsummary<?php echo $index % 2 == 0 ? ' grey' : ''; ?>">
	<div class="section-wrapper pricingsummary-wrapper">
		<h3 class="section-wrapper-superheader"><?php the_field('pricing_superheader', 'option'); ?></h3>
		<h1 class="section-wrapper-header pricingsummary-wrapper-header"><?php the_field('pricing_header', 'option'); ?></h1>
		<div class="pricingsummary-wrapper-badge">
			<h2 class="pricingsummary-wrapper-badge-header">Member</h2>
			<div class="pricingsummary-wrapper-badge-grid">
				<?php 
				if( have_rows('features', 'option') ):
					while( have_rows('features', 'option') ): the_row(); ?>
						<div class="pricingsummary-wrapper-badge-grid-item">	
							<?php the_sub_field('title'); ?>
						</div>
					<?php
					endwhile; 
				endif;
				?>
			</div>
			<div class="pricingsummary-wrapper-badge-price">
				<div class="pricingsummary-wrapper-badge-price-left">$1</div>
				<div class="pricingsummary-wrapper-badge-price-right">Today</div>
				<div class="pricingsummary-wrapper-badge-price-triangle"></div>
			</div>
		</div>
	</div>
</section>