<section class="section contactsection<?php echo $index % 2 == 0 ? ' grey' : ''; ?>">
	<div class="section-wrapper contactsection-wrapper">
		<h1 class="section-wrapper-header addonssummary-wraapper-header">Contact</h1>
		<?php echo do_shortcode( '[gravityform id="2" ajax="true" title="false" description="false"]' ); ?>
	</div>
</section>