<section class="homehero">
	<div class="homehero-textcontainer">
		<div class="homehero-textcontainer-header"><?php echo bloginfo('description'); ?></div>
		<div class="homehero-textcontainer-subheader"><?php echo 'Welcome to ' . get_bloginfo('sitename'); ?></div>
		<div class="homehero-textcontainer-buttons">
			<a href="<?php echo site_url('/gallery/') ?>" class="homehero-textcontainer-buttons-button">Site Gallery</a>
			<a href="<?php echo site_url('/contact/') ?>" class="homehero-textcontainer-buttons-button">Contact Us</a>
		</div>
	</div>
	<div class="homehero-tint"></div>
	<?php if( get_field('homehero_type_select', 'option') == 'video' ): ?>
		<video class="homehero-video" autoplay loop>
			<source src="<?php the_field('homehero_video', 'option') ?>" type="video/mp4">
		</video>
	<?php elseif ( get_field('homehero_type_select', 'option') == 'slideshow' ):
		$gallery = get_field('homehero_slideshow', 'option');
		if( $gallery ):
		?>
		<div class="homehero-slideshow">
			<?php foreach( $gallery as $index => $slide ): ?>
				<div class="homehero-slideshow-slide<?php echo $index == 0 ? ' first' : ''; ?>" style="background-image: url(<?php echo $slide['url'] ?>);"></div>
			<?php endforeach; ?>
		</div>
	<?php 
		endif;
	endif; ?>
</section>