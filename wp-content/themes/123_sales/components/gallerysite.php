<div class="gallerysite<?php echo isset($modal) && $modal ? ' modal' : ''; ?>">
	<nav class="gallerysite-nav">
		<div class="gallerysite-nav-pills">
			<span class="gallerysite-nav-pills-pill" data-type="phone">Phone</span>
			<span class="gallerysite-nav-pills-pill" data-type="tablet">Tablet</span>
			<span class="gallerysite-nav-pills-pill active" data-type="desktop">Desktop</span>
		</div>
	</nav>
	<iframe class="gallerysite-iframe desktop" src="<?php echo site_url('/spinner/'); ?>"></iframe>
	<?php if( isset($modal) && $modal ): ?>
		<div class="gallerysite-tint"></div>
		<i class="gallerysite-close fa fa-times"></i>
	<?php endif; ?>
</div>