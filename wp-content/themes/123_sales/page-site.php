<?php 
	if( !isset($_GET['id']) || empty($_GET['id']) ){
		header( 'Location: ' . site_url() );
	}

	get_header();

	$rows = UtilityBelt::get_all_gallery_site_meta();

	$the_row = [];

	foreach($rows as $row){
		if( $row['id'] == $_GET['id'] ){
			$the_row = $row;
		}
	}

	@include( locate_template( 'components/gallerysite.php' ) );

	get_footer();
?>