var APP = {};
;(function ( $, APP, window, document, undefined ) {

	$(document).ready(function(){

		// this is a global component used to determine which breakpoint we're at
		// to listen for the event do $(document).on('breakpoint', eventHandlerCallback);
		// callback accepts normal amount of params that a js event listener callback would have
		// but there's a custom property added to the event object called "name"
		// assuming in the callback the event object variable passed in is e: console.log(e.device);
		APP.Breakpoint = {
			name : '',
			_init : function(){
				$(window).on('resize load', APP.Breakpoint._resizeLoadHander);
			},
			_resizeLoadHander : function(){
				if( $(window).width() > 1024 && APP.Breakpoint.name != 'desktop' ){
					APP.Breakpoint.name = 'desktop';
					APP.Breakpoint._dispatchEvent();
				}
				else if( $(window).width() <= 1024 && $(window).width() > 640 && APP.Breakpoint.name != 'tablet' ){
					APP.Breakpoint.name = 'tablet';	
					APP.Breakpoint._dispatchEvent();
				}
				else if( $(window).width() < 641 && APP.Breakpoint.name != 'mobile' ){		
					APP.Breakpoint.name = 'mobile';		
					APP.Breakpoint._dispatchEvent();
				}
			},
			_dispatchEvent : function(){
				$(document).trigger($.Event('breakpoint', {device: APP.Breakpoint.name}));
			}
		}
		APP.Breakpoint._init();		

		APP.Parallax = function(image){
			this.image = image;
			this._map = function(n,i,o,r,t){return i>o?i>n?(n-i)*(t-r)/(o-i)+r:r:o>i?o>n?(n-i)*(t-r)/(o-i)+r:t:void 0;};
			var that = this;
			$(window).on('scroll load', function(e){
				$(that.image).css('transform', 'translate(0%, ' + that._map($(window).scrollTop(), 0, $(window).height(), 0, 30) + '%)')
			});
		}

		APP.MobileNav = {
			toggle : $('.mobilenav-wrapper-right-navtoggle'),
			tint : $('.mobilenav-tint'),
			nav : $('.nav'),
			_init : function(){
				$(window).on('scroll load', APP.MobileNav._scrollLoadHandler);
				APP.MobileNav.toggle.on('click', APP.MobileNav._clickHandler);
				$(document).on('breakpoint', APP.MobileNav._breakpointHandler);
			},
			_map : function(n,i,o,r,t){return i>o?i>n?(n-i)*(t-r)/(o-i)+r:r:o>i?o>n?(n-i)*(t-r)/(o-i)+r:t:void 0;},
			_scrollLoadHandler : function(e){
				var opacity = APP.MobileNav._map($(window).scrollTop(), 0, 300, 0, 1);
				APP.MobileNav.tint.css('opacity', opacity);
			},
			_clickHandler : function(){
				APP.MobileNav.nav.toggleClass('active');
				APP.MobileNav.toggle.toggleClass('fa-bars');
				APP.MobileNav.toggle.toggleClass('fa-times');
				$('body').toggleClass('navactive');
			},
			_breakpointHandler : function(e){
				if( e.device == 'desktop' ){
					APP.MobileNav.nav.removeClass('active');
					APP.MobileNav.toggle.addClass('fa-bars');
					APP.MobileNav.toggle.removeClass('fa-times');
					$('body').removeClass('navactive');		
				}
			}
		}
		APP.MobileNav._init();


		APP.DesktopNav = {
			nav : $('.nav'),
			_init : function(){
				$(window).on('scroll load', APP.DesktopNav._scrollLoadHandler);
			},
			_map : function(n,i,o,r,t){return i>o?i>n?(n-i)*(t-r)/(o-i)+r:r:o>i?o>n?(n-i)*(t-r)/(o-i)+r:t:void 0;},
			_scrollLoadHandler : function(){
				if( APP.Breakpoint.name == 'desktop' ){
					var opacity = APP.DesktopNav._map($(window).scrollTop(), 0, 300, 0, 1);
					APP.DesktopNav.nav.css('background-color', 'rgba(21,21,21,' + opacity + ')');
				}
			},
		}
		APP.DesktopNav._init();
		
		APP.Slideshow = {
			container : $('.hero-slideshow'),
			slides : $('.hero-slideshow-slide'),
			fadeSpeed : 2000,
			slideTime : 6000,
			_init : function(){
				if( APP.Slideshow.slides.length > 0 ){
					$(APP.Slideshow.slides[1]).addClass('second');
					APP.Slideshow._start();
					var parallax = new APP.Paralax(APP.Slideshow.container);
				}
			},
			_start : function(){
				setTimeout(APP.Slideshow._next, APP.Slideshow.slideTime);
			},
			_next : function(){
				$('.hero-slideshow-slide:first-child').fadeOut(APP.Slideshow.fadeSpeed, function(){
					var removed = $(this).detach();
					$(removed).css({
						'opacity' : 1,
						'display' : 'block',
					});
					$(removed).removeClass('first');
					APP.Slideshow.container.append(removed);
					$('.hero-slideshow-slide').first().addClass('first').removeClass('second');
					$('.hero-slideshow-slide:nth-child(2)').addClass('second');
				});
				APP.Slideshow._start();
			},
		}
		APP.Slideshow._init();

		APP.Video = {
			video : $('.homehero-video'),
			_init : function(){
				var parallax = new APP.Parallax(APP.Video.video);
			}
		}
		APP.Video._init();

		APP.Signup = {
			button : $('.nav-wrapper-menu-item.signup .nav-wrapper-menu-item-link'),
			popup : $('.signupsection'),
			close : $('.signupsection-wrapper-close'),
			fadeSpeed : 250,
			_init : function(){
				APP.Signup.button.on('click', APP.Signup._buttonClickHandler);
				APP.Signup.close.on('click', APP.Signup._close);
				APP.Signup.popup.on('click', APP.Signup._close);
			},
			_buttonClickHandler : function(e){
				e.preventDefault();
				APP.Signup.popup.fadeIn(APP.Signup.fadeSpeed);
			},
			_close : function(e){
				if( $(e.target).is('.signupsection, .signupsection-wrapper-close') ){
					APP.Signup.popup.fadeOut(APP.Signup.fadeSpeed);
				}
			},
		}
		APP.Signup._init();


		APP.Page = {
			bgs : $('.pagehero-imagecontainer-image'),
			_init : function(){
				$.each(APP.Page.bgs, function(index, el){
					var parallax = new APP.Parallax(el);
				});
			},
		}

		APP.Page._init();

		
		APP.GallerySite = {
			container : $('.gallerysite'),
			pills : $('.gallerysite-nav-pills-pill'),
			iframe : $('.gallerysite-iframe'),
			_init : function(){
				APP.GallerySite.pills.on('click', APP.GallerySite._clickHandler);
				APP.GallerySite.modal._init();
			},
			_clickHandler : function(e){
				APP.GallerySite.iframe.removeClass('phone tablet desktop');
				APP.GallerySite.iframe.addClass(e.target.dataset.type);
				APP.GallerySite.pills.removeClass('active');
				$(e.target).addClass('active');
			},
			_containerClickHandler : function(e){
				if( $(e.target).is('.gallerysite-tint, .gallerysite-close') ){
					APP.GallerySite.modal.open = false;	
					APP.GallerySite.container.fadeOut();
				}
			},
			modal : {
				gridItems : $('.galleryexposition-wrapper-grid-item, .gallerysummary-wrapper-grid-item'),
				open : false,
				_init : function(){
					if( APP.GallerySite.modal._hasModal() && APP.GallerySite.modal._hasGridItems() ){
						APP.GallerySite.modal.gridItems.on('click', APP.GallerySite.modal._clickHandler);
						APP.GallerySite.container.on('click', APP.GallerySite._containerClickHandler);
					}
				},
				_hasModal : function(){
					return $('.gallerysite.modal').length > 0;
				},
				_hasGridItems : function(){
					return APP.GallerySite.modal.gridItems.length > 0;
				},
				_clickHandler : function(e){
					if( APP.GallerySite.container.css('display') !== 'none' ){
						APP.GallerySite.container.fadeOut();	
					}
					else{
						APP.GallerySite.iframe.attr('src', e.target.dataset.url);
						APP.GallerySite.container.fadeIn();
					}
				},
			}
		}

		APP.GallerySite._init();


		APP.GalleryPage = {
			filter : $('.galleryexposition-filters-filter-ui'),
			grid : $('.galleryexposition-wrapper-grid'),
			gridItems : $('.galleryexposition-wrapper-grid-item'),
			spinner : $('.galleryexposition-wrapper-loading'),
			empty : false,
			hasNomoreMessage : false,
			isQuerying : false,
			_init : function(){
				APP.GalleryPage.filter.select2({
					placeholder: 'Select a category',
					allowClear : true,
				});
				APP.GalleryPage.filter.on('change.select2', APP.GalleryPage._selectFilterChange);
				APP.GalleryPage.grid = APP.GalleryPage.grid.isotope({
					itemSelector : APP.GalleryPage.gridItems.selector
				});
				$(window).on('scroll', APP.GalleryPage._scrollHandler);
			},
			_selectFilterChange : function(e){
				var id =  $(e.target).find(':selected')[0].dataset.id;
				if( typeof(id) !== 'undefined' ){
					APP.GalleryPage.grid.isotope({
						filter: APP.GalleryPage.gridItems.selector + '.' + id,
					});
				}
				else{
					APP.GalleryPage.grid.isotope({
						filter: APP.GalleryPage.gridItems.selector,
					});	
				}
			},
			_scrollHandler : function(){
				// if we're on the gallery exposition page
				if( $('.galleryexposition').length > 0 && APP.GalleryPage.hasNomoreMessage == false && APP.GalleryPage.isQuerying == false ){
					// if we're roughly near the bottom of the grid
					if( $(window).height() + $(window).scrollTop() > APP.GalleryPage.grid.offset().top + APP.GalleryPage.grid.height() + 100 ){
						APP.GalleryPage.spinner.removeClass('hide');
						console.log('trigger loadmore');
						var args = {
							action : 'gallery_request',
							offset : APP.GalleryPage.gridItems.length
						};
						if( typeof APP.GalleryPage.filter.find(':selected')[0].dataset.id == 'undefined' ){
							args.id = APP.GalleryPage.filter.find(':selected')[0].dataset.id;
						}
						if( APP.GalleryPage.empty == false ){
							APP.GalleryPage.isQuerying = true;
							$.post(WPAJAX, args, function(response){
								console.log(response);
								if( response == 'empty' ){
									APP.GalleryPage.empty = true;
								}
								else{
									APP.GalleryPage._renderGridItems(JSON.parse( response ));
								}
								APP.GalleryPage.spinner.addClass('hide');
								APP.GalleryPage.gridItems = $('.galleryexposition-wrapper-grid-item');
								APP.GalleryPage.isQuerying = false;
								$( document.body ).trigger( 'post-load' );
							});
						}
						else{
							if( APP.GalleryPage.hasNomoreMessage == false ){
								APP.GalleryPage.grid.after('<div class="galleryexposition-wrapper-grid-nomore">No more sites</div>')
								APP.GalleryPage.hasNomoreMessage = true;
								APP.GalleryPage.spinner.addClass('hide');
							}
						}
					}
				}
			},
			_renderGridItems : function(json){
				var els = '';
				for(var i in json){
					var data = json[i];
					var catString = '';
					for(var k in data.categories){
						catString += ' ' + data.categories[k].term_id;
					}
					var site_url = window.location.protcol + '//' + window.location.hostnmae;
					var el = '<div class="galleryexposition-wrapper-grid-item' + catString + '" style="background-image: url(' + data.image + ');">';
					for(var j in data.categories){
						el += '<div class="galleryexposition-wrapper-grid-item-cats">\
									<div class="galleryexposition-wrapper-grid-item-cats-cat">' + data.categories[j].name + '</div>\
								</div>';
					}
					
					el += '<div class="galleryexposition-wrapper-grid-item-text">' + data.title + '</div>\
						<div class="galleryexposition-wrapper-grid-item-tint"></div>\
					</div>';
					els += el;
				}
				els = $(els);
				APP.GalleryPage.grid.isotope().append(els).isotope( 'appended', els ).isotope('layout');
			}
		}

		APP.GalleryPage._init();

	});

})( jQuery, APP, window, document );

